import 'dart:io';

class calculate {
  var symbol;
  double num1 = 0;
  double num2 = 0;
  double sum = 0;

  calculate(double num1, double num2) {
    this.symbol = symbol;
    this.num1 = num1;
    this.num2 = num2;
    this.sum = 0;
  }

  void checkSymbol(var symbol) {
    switch (symbol) {
      case "+":
        sum = sum + add(num1, num2);
        break;
      case "-":
        sum = sum + sub(num1, num2);
        break;
      case "*":
        sum = sum + mul(num1, num2);
        break;
      case "/":
        sum = sum + divide(num1, num2);
        break;
      default:
        return;
    }
  }

  double setSum() {
    return sum = 0;
  }

  double getSum() {
    return sum;
  }

  double add(double num1, double num2) {
    return num1 + num2;
  }

  double sub(double num1, double num2) {
    return num1 - num2;
  }

  double mul(double num1, double num2) {
    return num1 * num2;
  }

  double divide(double num1, double num2) {
    return num1 / num2;
  }
}

void main() {
  var exit = false;
  print("press number: ");
  double num1 = double.parse(stdin.readLineSync()!);
  while (exit == false) {
    print("press the operator (press exit to quit): ");
    String? symbol = stdin.readLineSync();
    if (symbol == "exit") {
      break;
    }
    print("press number: ");
    double num2 = double.parse(stdin.readLineSync()!);
    calculate c1 = new calculate(num1, num2);
    c1.checkSymbol(symbol);
    print("Value = ${c1.getSum()}");
    num1 = c1.getSum();
  }
}
